package controller;

import api.IDivvyTripsManager;
import model.data_structures.IDoublyLinkedList;
import model.data_structures.Queue;
import model.logic.DivvyTripsManager;
import model.vo.VOTrip;

public class Controller<T> {
		
	
	/**
	 * Reference to the services manager
	 */
	@SuppressWarnings("unused")
	private static IDivvyTripsManager  manager = new DivvyTripsManager();
	
	private static String stationsFile = "./data/Divvy_Stations_2017_Q3Q4.csv";
	private static String tripsFile = "./data/Divvy_Trips_2017_Q3.csv";
	
	public static void loadStations() {

		manager.loadStations(stationsFile);
	}
	
	public static void loadTrips() {
		manager.loadTrips(tripsFile);
		
	}
		
	public static Queue <String> getLastNStations (int bicycleId, int n) {
		loadStations();
		return manager.getLastNStations(bicycleId, n);
	}
	
	public static VOTrip customerNumberN (int stationID, int n) {
		loadTrips();
		return manager.customerNumberN(stationID, n);
	}
}
