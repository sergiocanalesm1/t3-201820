package view;

import java.util.Scanner;

import controller.Controller;
import model.data_structures.IDoublyLinkedList;
import model.data_structures.Queue;
import model.vo.VOTrip;

public class DivvyTripsManagerView 
{
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin)
		{
			printMenu();
			
			int option = sc.nextInt();
			
			switch(option)
			{
				case 1:
					System.out.println("entro a view");
					Controller.loadStations();
					break;
					
				case 2:
					Controller.loadTrips();
					break;
					
				case 3:
					System.out.println("Ingrese el id de la bicicleta:");
					int bicycleId = Integer.parseInt(sc.next());
					
					System.out.println("Ingrese en n�mero de viajes:");
					int numberOfTrips = Integer.parseInt(sc.next());
					
					Queue<String> lastStations = Controller.getLastNStations (bicycleId, numberOfTrips);
					System.out.println("Las ultimas " + numberOfTrips + " estaciones: ");
					while (lastStations != null) 
					{
						System.out.println( lastStations.dequeue() );
					}
					break;
					
				case 4:
					System.out.println("Ingrese el identificador de la estaci�n:");
					int stationId = Integer.parseInt(sc.next());
					System.out.println("Ingrese el número del viaje que se quiere buscar:");
					int nTrip = Integer.parseInt(sc.next());
					
					VOTrip trip = Controller.customerNumberN(stationId, nTrip);
				
					if(trip == null) {
						System.out.println("No existe el viaje " + nTrip);
					} else {
						System.out.println(trip.id() + " (" + trip.getTripSeconds() + "): " + trip.getFromStationName() + "->" + trip.getToStationName());
					}
					
					break;
					
				case 5:	
					fin=true;
					sc.close();
					break;
			}
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 3----------------------");
		System.out.println("1. Cree una nueva coleccion de estaciones");
		System.out.println("2. Cree una nueva coleccion de viajes");
		System.out.println("3. Dar ultimas estaciones por las que ha pasado la bicicleta");
		System.out.println("4. Dar viaje numero N que llegó a la estacion");
		System.out.println("5. Salir");
		System.out.println("Digite el n�mero de opci�n para ejecutar la tarea, luego presione enter: (Ej., 1):");
		
	}
}
