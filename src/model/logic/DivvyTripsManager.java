package model.logic;

import java.io.BufferedReader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

import com.opencsv.*;


import api.IDivvyTripsManager;
import model.vo.VOByke;
import model.vo.VOTrip;
import model.data_structures.IDoublyLinkedList;
import model.data_structures.Queue;
import model.data_structures.Stack;

public class DivvyTripsManager implements IDivvyTripsManager {

	//atributos

	Queue<VOByke> colaStationsList;
	Queue<VOTrip> colaTripsList ;

	Stack<VOTrip> pilaTripsList;
	Stack<VOByke> pilaStationsList;

	//m�todos	

	public DivvyTripsManager(){
		
		colaStationsList = new Queue<>();
		colaTripsList = new Queue<>();
		
		pilaTripsList = new Stack<>();
		pilaStationsList = new Stack<>();
	}


	public void loadStations (String stationsFile) {
		try 
		{

			CSVReader reader = new CSVReader(new FileReader(stationsFile));


			String [] nextLine = reader.readNext();
			nextLine = reader.readNext();


			while (nextLine != null)
			{

				colaStationsList.enqueue(new VOByke(Integer.parseInt(nextLine[0]), nextLine[1] ,nextLine[2] ,nextLine[3] , nextLine[4] , Integer.parseInt(nextLine[5]),nextLine[6]));
				pilaStationsList.push(new VOByke(Integer.parseInt(nextLine[0]), nextLine[1] ,nextLine[2] ,nextLine[3] , nextLine[4] , Integer.parseInt(nextLine[5]),nextLine[6]));

				nextLine = reader.readNext();
			}
			reader.close();


		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	public void loadTrips (String tripsFile) {	
		
		int tripId;
		String startTime;
		String endTime;
		int bikeId;
		int tripDuration;
		int fromStationId;
		String fromStationName;
		int toStationId;
		String toStationName;
		String userType;
		String gender;
		int birthYear;
		
		try{
			


			CSVReader reader = new CSVReader(new FileReader(tripsFile));
			String [] nextLine = reader.readNext();
			nextLine = reader.readNext();		

			while(nextLine != null)
			{
				
				if( nextLine[0].equals("\"")){
					tripId = -1;//por defecto
				}else{
					tripId = Integer.parseInt(nextLine[0]);
				}				
				
				if(nextLine[1].equals("\""))
					startTime = "vac�o";
				else{
					startTime = nextLine[1];
				}
				
				if(nextLine[2].equals("\""))
					endTime = "vac�o";
				else{
					endTime = nextLine[2];
				}
				
				if( nextLine[3].equals("\"")){
					bikeId = -1;
				}else{
					bikeId = Integer.parseInt(nextLine[3]);
				}
				
				if( nextLine[4].equals("\""))
					tripDuration = -1;
				else{
					
					tripDuration = Integer.parseInt(nextLine[4]);
				}
				
				if(nextLine[5].equals("\"")){
					fromStationId = -1;
				}else{
					fromStationId = Integer.parseInt(nextLine[5]);
				}
				
				if( nextLine[6].equals("\""))
					fromStationName = "vac�o";
				else{
					fromStationName = nextLine[6];
				}
				
				if(nextLine[7].equals("\"")){
					toStationId = -1;
				}else{
					
					toStationId = Integer.parseInt(nextLine[7]);
					System.out.println(toStationId);
				}
				
				if( nextLine[8].equals("\""))
					toStationName = "vac�o";
				else{	
					
					toStationName = nextLine[8];
				}
						
				if(nextLine[9].equals("\""))
					userType = "vac�o";
				else{	
					userType = nextLine[9];
				}
				
				if( nextLine[10].equals("\""))
					gender = "vac�o";
				else{
					gender = nextLine[10].replaceAll("\"", "vacio");
					
				}
				
				if( nextLine[11].equals("\"")){
					birthYear = -1;
				}else{
					if(nextLine[11].isEmpty())
					{
						birthYear= -1;
					}
					else
					{
						
					birthYear = Integer.parseInt(nextLine[11]);
					}
					
				}

				VOTrip nuevo =  new VOTrip(tripId, startTime, endTime,bikeId,tripDuration,fromStationId,fromStationName,toStationId, toStationName,userType,gender,birthYear);				

				colaTripsList.enqueue(nuevo) ;				
				pilaTripsList.push(nuevo) ;

				nextLine = reader.readNext();
//				System.out.println("///////////////////////////");
//				for (int i = 0; i < nextLine.length; i++) 
//				{
//					System.out.println(nextLine[i]);
//				}

			}
			reader.close();
		}catch(FileNotFoundException e){
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}	
	}

	@Override
	public Queue<String> getLastNStations (int bicycleId, int n) {

		Queue<String> retornar = new Queue<>();

		int i = 0;		
		while(i <= n){

			VOTrip actual = colaTripsList.dequeue();
			if(actual.getBikeId() == bicycleId){
				i++;
				retornar.enqueue(actual.getFromStationName());				
			}
		}

		return retornar;
	}

	@Override
	public VOTrip customerNumberN (int stationID, int n) {

		int i = 0;
		VOTrip retornar = null;
		while (i < n){

			VOTrip actual = pilaTripsList.pop();
			
			if(actual.getToStationId() == stationID){
				i++;
				if(i == n){
					retornar = actual;
				}
			}
		}
		return retornar;

	}

}



