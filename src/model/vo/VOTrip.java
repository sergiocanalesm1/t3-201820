package model.vo;

/**
 * Representation of a Trip object
 */
public class VOTrip {
	
	
	//atributos
	
	private int tripId;
	private String startTime;
	private String endTime;
	private int bikeId;
	private int tripDuration;
	private int fromStationId;
	private String fromStationName;
	private int toStationId;
	private String toStationName;
	private String userType;
	private String gender;
	private int birthYear;
	
	

	//constructor
	
	public VOTrip(int tripId,	String startTime,	String endTime,	int bikeId,	int tripDuration,	int fromStationId,	
			String fromStationName,	int toStationId,	String toStationName,String userType,String gender,
			int birthYear)
	{
		this.tripId = tripId;
		this.startTime = startTime;
		this.endTime = endTime;
		this.bikeId = bikeId;		
		this.tripDuration = tripDuration;		
		this.fromStationId = fromStationId;
		this.fromStationName = fromStationName;
		this.toStationId = toStationId;
		this.toStationName = toStationName;
		this.userType = userType;
		this.gender = gender;
		this.birthYear = birthYear;
	}
	
	/**
	 * @return id - Trip_id
	 */
	public int id() {
		return tripId;
	}	
	
	public int getBikeId(){
		return bikeId;
	}
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		
//		String[] posiciones = tripDuration.split(":");
//		
//		int horas = Integer.parseInt(posiciones[0]);
//		int minutos = Integer.parseInt(posiciones[1]);
//		int segundos = Integer.parseInt(posiciones[2]);
//		
//		horas *= 3600;
//		minutos *= 60;
//		segundos += (minutos + horas);
//		
//		return segundos ;
		return tripDuration;
	}

	/**
	 * @return station_name - Origin Station Name .
	 */
	public String getFromStationName() {
		// TODO Auto-generated method stub
		return fromStationName;
	}
	
	
	/**
	 * @return station_name - Destination Station Name .
	 */
	public String getToStationName() {
		// TODO Auto-generated method stub
		return toStationName;
	}
	public String getGender(){
		return gender;
	}

	public int getToStationId() {
		// TODO Auto-generated method stub
		return toStationId;
	}
}

