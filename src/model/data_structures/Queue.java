package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Queue <T > implements IQueue //extends comparable
{
	//-------------------------------------
	//Atributos
	//-------------------------------------

	/**
	 * Nodo gen�rico que representa el primero de la cola
	 */
	private Node <T> cabeza;

	/**
	 * Node gen�rico que representa el �ltimo de la cola
	 */
	private Node <T> ultimo;

	/**
	 * el tama�o de la cola.
	 */
	private int tamanio;

	//-------------------------------------
	//Constructor
	//-------------------------------------

	/**
	 * constructor de la lista.
	 * inicializa a cabeza = null, ultimo = null, tamanio = 0
	 */
	public Queue ()
	{
		cabeza = null;
		ultimo = null;
		tamanio = 0;


	}

	/**
	 * da el Node que est� guardado en la primera posici�n de la lista
	 * @return cabeza
	 */
	public Node <T> darCabeza ()
	{
		return cabeza;
	}

	/**
	 * da el Node que est� guardado en la ultima posici�n de la lista
	 * @return ultimo
	 */

	public Node <T> darUltimo ()
	{
		return ultimo;
	}

	/**
	 * retorna el tama�o de la lista
	 * @return tamanio
	 */
	public int size()
	{
		return tamanio;
	}

	/**
	 * metodo que agrega un elemento a la cola en la �ltima posicion
	 */
	public void enqueue (T t)
	{
		Node <T> nuevoNode = new Node<>(t);

		if(isEmpty())
		{
			cabeza = nuevoNode;
			ultimo = nuevoNode;
		}

		else {
			//viejo ultimo es viejo actual

			Node<T> viejoUltimo = ultimo;
			viejoUltimo.setNextNode(nuevoNode);
			ultimo = nuevoNode;
		}
		tamanio++;
	}

	/**
	 * metodo que quita el cabeza en la lista y reduce el tama�o de la lista
	 */
	public T dequeue () 
	{
		if(isEmpty()){
			System.out.println("no deber�a estar vac�o");
		}


		T elemento = cabeza.getItem();
		Node <T> anteriorcabeza = cabeza;
		cabeza = anteriorcabeza.getNext();
		anteriorcabeza.setNextNode(null);

		tamanio--;
		
	
		return elemento;
	}

	/**
	 * metodo que me dice si est� vacia o no una 
	 * @return 
	 */
	public boolean isEmpty()
	{
		return (tamanio == 0);

	}

	@Override
	public Iterator<T> iterator()  {
		return new ListIterator();  
	}

	//sacado de https://algs4.cs.princeton.edu/13stacks/LinkedQueue.java.html

	private class ListIterator implements Iterator<T> {
		private Node current = cabeza;
		
		public boolean hasNext(){ 
			return current != null;
		}
		
		
		public T next() {
			if (!hasNext()) 
				throw new NoSuchElementException();
			
			T item = (T) current.getItem();
			current = current.getNext(); 
			return item;
		}
	}



}
