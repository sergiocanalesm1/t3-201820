package model.data_structures;

//import java.util.Iterator;

public class Node<T>  {
	
	//atributos
	
	private Node<T> next;
	private  T item;
//	private Node<T> previous;
	
	//constructor
	
	public Node (T item)
	{
		//previous = null;
		next = null;
		this.item = item;
	}
	//m�todos
	
	public Node<T> getNext()
	{
		return next;
	}
	
//	public Node<T> getPrev(){
//		return previous;
//	}
	
//	public void setPrevNode(Node<T> prev){
//		this.previous = prev;
//	}
	public void setNextNode(Node<T> next)
	{
		this.next = next;
	}
	
	public void setItem(T item)
	{
		this.item = item;
	}
	
	public T getItem()
	{
		return item;
	}


}