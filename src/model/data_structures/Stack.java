package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

//import model.data_structures.Queue.ListIterator;

@SuppressWarnings("rawtypes")
public class Stack<T> implements IStack{

	//-------------------------------------

	//Atributos

	//-------------------------------------



	private Node <T> superior;
	private int tamanio;


	//-------------------------------------

	//Constructor

	//-------------------------------------



	public Stack()
	{
		superior = null;
		tamanio = 0;
	}

	/**

	 * Retorna true si la Pila esta vacia

	 * @return true si la Pila esta vacia, false de lo contrario

	 */

	public boolean isEmpty()

	{
		//		boolean rta;
		//		if (tamanio ==0)
		//		{
		//			rta = true;
		//		}
		//		else
		//		{
		//			rta = false;
		//		}
		//		return rta;
		return (tamanio == 0);
	}
	/**

	 * Retorna el numero de elementos contenidos

	 * @return el numero de elemntos contenidos

	 */

	public int size()
	{
		return tamanio;

	}

	/**

	 * Inserta un nuevo elemento en la Pila

	 * @param t el nuevo elemento que se va ha agregar

	 */

	public void push(T t)

	{

		Node <T> nuevoNodo = new Node<T> (t);

		if(isEmpty())
		{
			superior = nuevoNodo;
		}
		else
		{
			nuevoNodo.setNextNode(superior);

			superior = nuevoNodo;
		}

		tamanio++;
	}



	/**

	 * Quita y retorna el elemento agregado más recientemente

	 * @return el elemento agregado más recientemente o null si est� vacia la pila

	 */

	public T pop()

	{

		if (isEmpty() == true)

		{

			return null;

		}

		else

		{
			Node <T> antiguoSuperior = superior;

			T respuesta = superior.getItem();

			superior = null;
			superior = antiguoSuperior.getNext();

			antiguoSuperior.setNextNode(null);

			tamanio--;

			return respuesta;

		}

	}

	@Override
	public Iterator<T> iterator()  {
		return new ListIterator();  
	}

	//sacado de https://algs4.cs.princeton.edu/13stacks/LinkedQueue.java.html

	private class ListIterator implements Iterator<T> {
		private Node current = superior;

		public boolean hasNext(){
			return current != null;
		}


		public T next() {
			if (!hasNext()) throw new NoSuchElementException();
			@SuppressWarnings("unchecked")
			T item = (T) current.getItem();
			current = current.getNext(); 
			return item;
		}
	}


}
