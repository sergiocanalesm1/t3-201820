package UnitTest;

import static org.junit.Assert.*;

import org.junit.Test;

import model.data_structures.Stack;

public class StackTest {
	
	private Stack<Integer> testStack;
	
	public void setupScenario1(){
		testStack = new Stack<>();
		
		for(int i = 0; i < 10 ; i++){			
			testStack.push(i);
		}
		
	}
	
	@Test
	public void test(){
		setupScenario1();
		
		assertEquals("no coincide el tama�o" , 10 , testStack.size()); //este revisa tanto el pop como el push
		assertEquals("se est�n cargando mal los datos" , "9" , "" + testStack.pop());
		
	}

}
