package UnitTest;
import static org.junit.Assert.*;

import org.junit.Test;

import api.IDivvyTripsManager;
import model.logic.DivvyTripsManager;
import model.vo.VOTrip;

public class ControllerTest {
	
	private static IDivvyTripsManager  manager;
	private String testFile = "./data/trips_test.csv";
	private int toStationId = 111;
	private VOTrip tested = new VOTrip (17536697,"12/31/2017 23:42","12/31/2017 23:47",5353,272,240,"Sheridan Rd & Irving Park Rd",toStationId,"Clarendon Ave & Junior Ter","Subscriber","Male",1977);
	
	private void setupScenario1(){
		
		manager = new DivvyTripsManager();		
		manager.loadTrips(testFile);		
	}
	
	@Test
	public void test() {
		
		setupScenario1();
		assertEquals("el m�todo getcustomerNumber no est� corriendo bien" , tested , manager.customerNumberN(toStationId, 1));
		assertEquals("no sisrve el m�todo getLastNStations" , manager.getLastNStations(5353	, 3).dequeue() , tested.getFromStationName());
		
	}
	

}
