
package UnitTest;
import static org.junit.Assert.*;
import java.util.concurrent.ArrayBlockingQueue;
import org.junit.Test;

import junit.framework.TestCase;
import model.data_structures.Queue;
import model.data_structures.Stack;


public class QueueTest {


	private Queue<Integer> testQueue; 
	

	public void setupScenario1(){

		
		testQueue = new Queue<>();
		for(int i = 0 ; i < 10 ; i++){
			testQueue.enqueue(i);
		}
		testQueue.dequeue();
		
	}
		@Test
		public void test() {
			setupScenario1();
			assertEquals("se carg� mal la cola" , 9 , testQueue.size()); //aqu� se evalua que carge los 10 elementos y que se le haya quitado el primero
			assertEquals("se est�n cargando mal los datos" , "1" ,"" +  testQueue.dequeue());
		}

	}
